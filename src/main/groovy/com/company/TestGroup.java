package com.company;

import java.util.ArrayList;
import java.util.List;

public class TestGroup {
    public int id;
    public String name;
    private List<TestCase> testCases = new ArrayList<>();

    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

    @Override
    public String toString() {
        return "Test Group " + id + " " + name + " (" + testCases.size() + " cases)";
    }
}
