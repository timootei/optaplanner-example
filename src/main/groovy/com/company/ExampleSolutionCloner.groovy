package com.company

import org.optaplanner.core.api.domain.solution.cloner.SolutionCloner

class ExampleSolutionCloner implements SolutionCloner<ExampleSolution> {
    @Override
    ExampleSolution cloneSolution(ExampleSolution original) {

        def cloneSolution = new ExampleSolution()
        def identityCloneMap = new IdentityHashMap<Object, Object>()
        def groups = new HashMap<Integer, TestGroup>()

        def personCloner = { Person person ->
            def clonedTestCases = new ArrayList<TestCase>(person.testCases.size())
            person.testCases.each { TestCase t ->
                def clonedTestCase = (TestCase) identityCloneMap.get(t)
                if (clonedTestCase == null) {
                    throw new RuntimeException("Didn't find testcase in clone map")
                }
                clonedTestCases.add(clonedTestCase)
            }

            return new Person(
                    name: person.name,
                    testCases: clonedTestCases
            )
        }

        def clonedTestCases = new ArrayList(original.testCases.size())
        for (def testCase : original.testCases) {
            def clonedGroup = identityCloneMap
                    .computeIfAbsent(testCase.testGroup, { TestGroup group -> new TestGroup(id: group.id, name: group.name) })
            def clonedTestCase =
                    new TestCase(
                            id: testCase.id,
                            testGroup: clonedGroup)
            clonedGroup.testCases.add(clonedTestCase)
            clonedTestCases.add(clonedTestCase)
            identityCloneMap.put(testCase, clonedTestCase)
        }

        def clonedPersons = new ArrayList<Person>(original.persons.size())
        for (def agent : original.persons) {
            clonedPersons.add((Person)identityCloneMap.computeIfAbsent(agent, personCloner))
        }

        // second round to update circular-ref-based agents
        original.testCases.each {
            def clone = (TestCase) identityCloneMap.get(it)

            if (it.person == null) {
                return
            }

            clone.person = (Person)identityCloneMap.computeIfAbsent(it.person, personCloner)
        }

        cloneSolution.testCases = clonedTestCases
        cloneSolution.persons = clonedPersons
        cloneSolution.score = original.score

        return cloneSolution
    }
}
