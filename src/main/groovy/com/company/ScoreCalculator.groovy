package com.company

import org.optaplanner.core.api.score.Score
import org.optaplanner.core.api.score.buildin.hardmediumsoftlong.HardMediumSoftLongScore
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator

class ScoreCalculator implements EasyScoreCalculator<ExampleSolution> {
    @Override
    Score calculateScore(ExampleSolution solution) {
        def hardScore = 0
        def softScore = 0
        def MAX_CASES = 30

        for (TestCase testCase : solution.testCases) {
            Person person = testCase.person

            if (person == null) {
                softScore -= 10000
                continue
            }

            if (person.testCases.size() > MAX_CASES) {
                hardScore -= (person.testCases.size() - MAX_CASES)
            }

            def uniquePersonsPerGroup = testCase.testGroup.testCases
                    .findAll { it.person != null }.collect { it.person.name}.unique().size()
            if (uniquePersonsPerGroup > 1) {
                softScore -= (uniquePersonsPerGroup - 1) * 100
            }
        }

        return HardSoftScore.valueOf(hardScore, softScore)
    }
}
