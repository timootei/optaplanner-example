package com.company

import java.util.ArrayList
import java.util.Arrays
import java.util.List
import org.optaplanner.core.api.solver.Solver
import org.optaplanner.core.api.solver.SolverFactory

class Main {

    static void main(String[] args) {
        SolverFactory<ExampleSolution> solverFactory = SolverFactory.createFromXmlResource("com/company/example.xml")

        List<Person> persons = (1..4).collect {
            new Person(name: "p${it}")
        }

        def groups = (0..9).collect { new TestGroup(id: it, name: it.toString()) }.collectEntries { [it.id, it] }
        def tests = (1..59).collect {
            def group = groups[(int) (it / 30)]
            def test = new TestCase(id: it, testGroup: group)
            group.testCases.add(test)

            return test
        }

        ExampleSolution initialSolution = new ExampleSolution()
        initialSolution.setPersons(persons)
        initialSolution.setTestCases(tests)

        Solver<ExampleSolution> solver = solverFactory.buildSolver()

        ExampleSolution plannedSolution = solver.solve(initialSolution)

        plannedSolution.testCases.each {
            println "${it.id} (${it.testGroup.id}) -> ${it.person?.name}"
        }
    }
}
