package com.company;

import java.util.ArrayList;
import java.util.List;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.InverseRelationShadowVariable;

@PlanningEntity
public class Person {
    public String name;

    private List<TestCase> testCases = new ArrayList<>();

    @InverseRelationShadowVariable(sourceVariableName = "person")
    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCase> testCases) {
        this.testCases = testCases;
    }

    @Override
    public String toString() {
        return "Person: " + name;
    }
}
